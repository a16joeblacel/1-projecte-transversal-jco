DELETE FROM reserva;
DELETE FROM sessio;
DELETE FROM client;
DELETE FROM pelicula;
DELETE FROM admin;

INSERT INTO pelicula (nom, imatge, descripcion, trailer) VALUES ("RompeRalph", "img/romperalph.jpg", "Ralph sale de los recreativos y ¡rompe Internet! ¿Qué podría salir mal? La secuela de ¡Rompe Ralph! (2012) tendrá lugar en el presente, seis años después de los acontecimientos de la primera película. En ella volveremos a ver a Ralph, el malo del videojuego que quiere ser un héroe, junto a otros personajes ya conocidos como Vanellope von Schweetz, la joven error de programación co un imparable espíritu ganador. 
La historia se centrará en las aventuras de Ralph y sus compañeros en el mundo de Internet, a dónde irán para intentar salvar Sugar Rush. Allí se encontrarán con lo más temido de la Red: los pop ups, los anuncios, los memes y el spam; pero también con personajes tan míticos como las princesas Disney, Buzz Lightyear o C3PO. Por suerte no estarán solos, ya que el algoritmo azulado Yess ayudará a nuestros dos protagonistas a cumplir su objetivo. ", "https://www.youtube.com/embed/DWwlwZpzI5s");

INSERT INTO pelicula (nom, imatge, descripcion, trailer) VALUES ("Superlopez", "img/superlopez.jpeg", "Superlópez (Dani Rovira) nació en el planeta Chitón con el nombre de Jo-Con-Él y, después de colarse en un cohete, consiguió llegar hasta la Tierra siendo todavía un bebé. Tras ser encontrado cerca de Lérida, es adoptado por el matrimonio López, que decide llamar al chico Juan López. Juan crece como un humano más, al tiempo que se esfuerza por controlar sus superpoderes y combatir el mal. Ya adulto, comienza a trabajar como contable en una oficina en Barcelona. Sometido a la presión de su jefe y a los antojos de su dominante novia Luisa Lanas (Alexandra Jiménez), se evade de la rutina diaria ejerciendo de superhéroe bajo el nombre de Superlópez. ", "https://www.youtube.com/embed/8BvwQGeqreI");

INSERT INTO pelicula (nom, imatge, descripcion, trailer) VALUES ("El Grinch", "img/grinch.jpeg", "El Grinch es un ser gruñón y peludo de color verde que vive en una cueva oculta en lo alto de la montaña. Él mismo asegura que su corazón es muy pequeño y que ese es el motivo por el cual es un cascarrabias que siempre está quejándose de todo y no hay nada que le parezca bien. Su único amigo es un perrito llamado Max, y esto para el Grinch es más que suficiente porque él no necesita a nadie.
Un día, desde su guarida, en lo alto del Monte Crumpit, empieza a escuchar ruidos procedentes de la ciudad de Villaquien, donde sus habitantes, los Quien, se encuentran entusiasmados organizando los preparativos para celebrar la Navidad. A él tanta alegría le provoca una rabia y una envidia que es incapaz de controlar. Pero, pronto descubrirá qué puede hacer para que ese sentimiento desaparezca: bajar a la localidad y allí robar todos los adornos y los regalos navideños. Así nadie podrá disfrutar de esa festividad.", "https://www.youtube.com/embed/IY7zGfXegTk");

INSERT INTO pelicula (nom, imatge, descripcion, trailer) VALUES ("Venom", "img/venom.jpeg", "Eddie Brock (Tom Hardy) es un consolidado periodista y astuto reportero que está investigando una empresa llamada Fundación Vida. Esta fundación, dirigida por el eminente científico Carlton Drake (Riz Ahmed), está ejecutando secretamente experimentos ilegales en seres humanos y realizando pruebas que involucran formas de vida extraterrestres y amorfas conocidas como simbiontes. Durante una visita furtiva a la central, el periodista quedará infectado por un simbionte. Comenzará entonces a experimentar cambios en su cuerpo que no entiende, y escuchará una voz interior, la del simbionte Venom, que le dirá lo que tiene que hacer. Cuando Brock adquiera los poderes del simbionte que le usa como huésped, Venom tomará posesión de su cuerpo, convirtiéndole en un despiadado y peligroso súpervillano.", "https://www.youtube.com/embed/nZ8FXOpcXSs");

INSERT INTO pelicula (nom, imatge, descripcion, trailer) VALUES ("Mirage", "img/mirage.jpg", "Tres jóvenes parejas deciden pasar un fin de semana en el desierto. Al principio, todo marcha con buen pie, todo es diversión y buenas experiencias. Observar desde la sombra cómo se divierte este grupo de amigos, será la principal obsesión de un misterioso hombre (B.G. Steers).
El fin de semana idílico, se tiñede tintes dramáticos cuando descubren que el hombre que les vigila tiene planeada su propia diversión, algo que ellos ni siquiera llegan a imaginar. En ese momento comienza una caza que lleva a los protagonistas a recurrir a todo tipo de medidas desesperadas  en una batalla por sobrevivir en pleno desierto.", "https://www.youtube.com/embed/ESuEBn7WAUE");

INSERT INTO client (email, nom, cognom, dataNaixement, telefon) VALUES ("a16joeblacel@iam.cat", "Joel", "Blanco", "1998-04-21", 654314564);
INSERT INTO client (email, nom, cognom, dataNaixement, telefon) VALUES ("a15crisfer@iam.cat", "Cristina", "Ferrer", "1997-11-02", 698124354);
INSERT INTO client (email, nom, cognom, dataNaixement, telefon) VALUES ("a15orisol@iam.cat", "Oriol", "Soler", "1997-09-15", 676780614);

INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-1", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-2", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-3", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-4", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-5", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-6", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-7", 2);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-8", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-9", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-10", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-11", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-12", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-13", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-14", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-15", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-16", 2);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-17", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-18", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-19", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-20", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-21", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-22", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-23", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-24", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("18.00", "2018-12-25", 5);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("16.00", "2018-12-26", 2);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-27", 4);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-28", 1);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-29", 2);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-30", 3);
INSERT INTO sessio (hora, data, id_pelicula) VALUES ("20.00", "2018-12-31", 5);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 3, 4, 1, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 4, 1, 3, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 5, 6, 2, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "V", 6, 4, 3, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 7, 3, 2, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 8, 4, 3, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 3, 5, 2, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 1, 4, 1, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 2, 4, 1, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 3, 6, 2, 12);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-12", "N", 3, 9, 3, 12);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 3, 4, 1, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 4, 1, 3, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 5, 6, 2, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "V", 6, 4, 3, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 7, 3, 2, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 8, 4, 3, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 3, 5, 2, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 1, 4, 1, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 2, 4, 1, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 3, 6, 2, 13);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-13", "N", 3, 8, 3, 13);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 3, 4, 1, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 4, 1, 3, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 5, 6, 2, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "V", 6, 4, 3, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 7, 3, 2, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 8, 4, 3, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 3, 5, 2, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 1, 4, 1, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 2, 4, 1, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 3, 6, 2, 14);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-14", "N", 3, 8, 3, 14);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 3, 4, 1, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 4, 1, 3, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 5, 6, 2, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "V", 6, 4, 3, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 7, 3, 2, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 8, 4, 3, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 3, 5, 2, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 1, 4, 1, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 2, 4, 1, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 3, 6, 2, 15);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-15", "N", 3, 8, 3, 15);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 3, 4, 1, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 4, 1, 3, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 5, 6, 2, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "V", 6, 4, 3, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 7, 3, 2, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 8, 4, 3, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 3, 5, 2, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 1, 4, 1, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 2, 4, 1, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 3, 6, 2, 16);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-16", "N", 3, 8, 3, 16);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 3, 4, 1, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 4, 1, 3, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 5, 6, 2, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "V", 6, 4, 3, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 7, 3, 2, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 8, 4, 3, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 3, 5, 2, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 1, 4, 1, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 2, 4, 1, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 3, 6, 2, 17);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-17", "N", 3, 8, 3, 17);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 3, 4, 1, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 4, 1, 3, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 5, 6, 2, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "V", 6, 4, 3, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 7, 3, 2, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 8, 4, 3, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 3, 5, 2, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 1, 4, 1, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 2, 4, 1, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 3, 6, 2, 18);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-18", "N", 3, 8, 3, 18);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 3, 4, 1, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 4, 1, 3, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 5, 6, 2, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "V", 6, 4, 3, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 7, 3, 2, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 8, 4, 3, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 3, 5, 2, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 1, 4, 1, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 2, 4, 1, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 3, 6, 2, 19);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-19", "N", 3, 8, 3, 19);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 3, 4, 1, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 4, 1, 3, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 5, 6, 2, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "V", 6, 4, 3, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 7, 3, 2, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 8, 4, 3, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 3, 5, 2, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 1, 4, 1, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 2, 4, 1, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 3, 6, 2, 20);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-20", "N", 3, 8, 3, 20);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 3, 4, 1, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 4, 1, 3, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 5, 6, 2, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "V", 6, 4, 3, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 7, 3, 2, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 8, 4, 3, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 3, 5, 2, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 1, 4, 1, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 2, 4, 1, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 3, 6, 2, 21);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-21", "N", 3, 8, 3, 21);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 3, 4, 1, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 4, 1, 3, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 5, 6, 2, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "V", 6, 4, 3, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 7, 3, 2, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 8, 4, 3, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 3, 5, 2, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 1, 4, 1, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 2, 4, 1, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 3, 6, 2, 22);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-22", "N", 3, 8, 3, 22);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 3, 4, 1, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 4, 1, 3, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 5, 6, 2, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "V", 6, 4, 3, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 7, 3, 2, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 8, 4, 3, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 3, 5, 2, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 1, 4, 1, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 2, 4, 1, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 3, 6, 2, 23);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-23", "N", 3, 8, 3, 23);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 3, 4, 1, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 4, 1, 3, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 5, 6, 2, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "V", 6, 4, 3, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 7, 3, 2, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 8, 4, 3, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 3, 5, 2, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 1, 4, 1, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 2, 4, 1, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 3, 6, 2, 24);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-24", "N", 3, 8, 3, 24);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 3, 4, 1, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 4, 1, 3, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 5, 6, 2, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "V", 6, 4, 3, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 7, 3, 2, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 8, 4, 3, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 3, 5, 2, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 1, 4, 1, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 2, 4, 1, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 3, 6, 2, 25);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-25", "N", 3, 8, 3, 25);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 3, 4, 1, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 4, 1, 3, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 5, 6, 2, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "V", 6, 4, 3, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 7, 3, 2, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 8, 4, 3, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 3, 5, 2, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 1, 4, 1, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 2, 4, 1, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 3, 6, 2, 26);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-26", "N", 3, 8, 3, 26);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 3, 4, 1, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 4, 1, 3, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 5, 6, 2, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "V", 6, 4, 3, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 7, 3, 2, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 8, 4, 3, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 3, 5, 2, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 1, 4, 1, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 2, 4, 1, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 3, 6, 2, 27);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-27", "N", 3, 8, 3, 27);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 3, 4, 1, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 4, 1, 3, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 5, 6, 2, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "V", 6, 4, 3, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 7, 3, 2, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 8, 4, 3, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 3, 5, 2, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 1, 4, 1, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 2, 4, 1, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 3, 6, 2, 28);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-28", "N", 3, 8, 3, 28);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 3, 4, 1, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 4, 1, 3, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 5, 6, 2, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "V", 6, 4, 3, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 7, 3, 2, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 8, 4, 3, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 3, 5, 2, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 1, 4, 1, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 2, 4, 1, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 3, 6, 2, 29);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-29", "N", 3, 8, 3, 29);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 3, 4, 1, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 4, 1, 3, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 5, 6, 2, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "V", 6, 4, 3, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 7, 3, 2, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 8, 4, 3, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 3, 5, 2, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 1, 4, 1, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 2, 4, 1, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 3, 6, 2, 30);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-30", "N", 3, 8, 3, 30);

INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 3, 4, 1, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 4, 1, 3, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 5, 6, 2, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "V", 6, 4, 3, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 7, 3, 2, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 8, 4, 3, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 3, 5, 2, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 1, 4, 1, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 2, 4, 1, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 3, 6, 2, 31);
INSERT INTO reserva (data, tipus, fila, columna,id_client, id_sessio) VALUES ("2018-12-31", "N", 3, 8, 3, 31);

INSERT INTO admin (dia) values ("2018-12-18");
INSERT INTO admin (dia) values ("2018-12-22");
INSERT INTO admin (dia) values ("2018-12-25");


