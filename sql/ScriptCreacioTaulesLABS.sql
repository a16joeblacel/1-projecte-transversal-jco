use a15orisolber_cinema;

DROP TABLE IF EXISTS reserva;
DROP TABLE IF EXISTS sessio;
DROP TABLE IF EXISTS pelicula;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS admin;


CREATE TABLE client (id integer AUTO_INCREMENT primary key, email varchar(30) UNIQUE, nom varchar(15), cognom varchar(15), dataNaixement date, telefon integer);

CREATE TABLE pelicula (id integer AUTO_INCREMENT primary key, nom varchar(20), imatge varchar(20), descripcion varchar(1000), trailer varchar(500));

CREATE TABLE sessio (id_sessio integer AUTO_INCREMENT primary key, hora varchar(10), data date, id_pelicula integer, foreign key (id_pelicula) references pelicula(id));

CREATE TABLE reserva (id integer AUTO_INCREMENT primary key, data date, tipus varchar(10), fila integer, columna integer, id_client integer, id_sessio integer, foreign key (id_client) references client(id), foreign key (id_sessio) references sessio(id_sessio));

CREATE table admin (id int NOT null AUTO_INCREMENT PRIMARY KEY,  dia date);
