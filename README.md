# 1 Projecte Transversal JCO

##Integrants:
 Joel Blanco, Cristina Ferrer, Oriol Soler.

##Objectiu
Disseny i codificació d'una web funcional per un cine. Una pel·lícula i sessió per día.

##Estat
Prácticament acabat

##Adreça del full de càlcul
https://camp.iam.cat/mod/url/view.php?id=42523

##Adreça de la documentació
<http://labs.iam.cat/~a15orisolber/doc>
##Desplegament labs:
<http://labs.iam.cat/~a15orisolber/src>
