<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Actualidad - Cinema JCO</title>

  <?php include 'php/infopeli.php';?>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/master.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">

  <script src="../js/jquery-3.3.1.js"></script>

  <script src="../js/calendari.js"></script>

</head>

<body class="white actualidad">
  <!-- Main - Padre -->
  <div class="container gris sombra">
    <!-- Cabecera -->
    <header class="white twelve columns">
      <!-- Logo del cine-->
      <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO"></a>
      </div>
      <!-- Nav -->
      <nav class="ten columns">
        <div class="twelve ">
          <?php include '../includes/nav.php'?>
        </div>
      </nav>
    </header>
    <section class="twelve columns gris">
      <article class="four columns white">
        <img alt="Marvel Ant" class="twelve columns" src="http://static.hellofriki.com/wp-content/uploads/2015/09/Marvel-Ant-Man-Banner-Poster.jpg" />
        Este verano de 2015 ha sido la fecha elegida para que Disney llevase a su fin, la segunda fase de su franquicia
        Marvel. Para ello se ha optado por adaptar a un personaje que no se encuentra dentro del top de los superheroes
        mas conocidos.
        Pero a pesar de no ser una de las caras con más adeptos del universo ideado por Stan Lee, el proyecto contó con
        el apoyo de Disney desde el principio, tanto en medios como en presupuesto.
      </article>
      <article class="four columns white">
        <img alt="Framestore" class="twelve columns" src="https://img.ecartelera.com/noticias/fotos/42400/42444/1.jpg" />
        La empresa londinense Framestore acaba de publicar un vídeo revelando el making of de algunos de los efectos
        visuales. En el vídeo, encontramos los entramados digitales de algunos de los escenarios galácticos donde
        transcurren las aventuras de la patrulla de superhéroes. Además, vemos cómo personajes como Groot (interpretado
        por Vin Diesel) cobran vida a partir de la captura del movimiento de los actores.
      </article>
      <article class="four columns white">
        <img alt="Assassins Creed" class="twelve columns" src="https://im.ziffdavisinternational.com/ign_es/screenshot/default/ac-banner_nwtn.jpg" />
        La película basada en la celebérrima saga de videojuegos de Ubisoft ha presentado un nuevo banner, que podéis
        ver a continuación, y ha estrenado una curiosa campaña viral.
        Ya se ha inaugurado de manera oficial la página web de Abstergo, la corporación sobre la que gira la trama la
        saga, que proporciona viajes virtuales al pasado a través de los recuerdos genéticos.La cuenta oficial de
        Twitter del filme nos ha dejado una imagen en la que podemos ver la tarjeta de presentación de Alan Rikkin, CEO
        de Abstergo.
      </article>
    </section>

    <section class="twelve columns gris">
      <article class="four columns white">
        <img alt="Avatar" class="twelve columns" src="https://www.netambulo.com//wp-content/uploads/2013/11/avatar.jpg" />
        Esta semana vuelve a estar de moda la peli Avatar por su estreno en televisión a pesar de la mala idea de
        Telecinco de partir la película en dos días para ganar audiencia y cargarse la continuidad de la historia.
        La película de James Cameron intentó crear un antes un después en el cine 3D, sin embargo, fue otra película
        más en este sentido. Nadie niega que visualmente es una película muy luminosa y colorida, pero, en el terreno
        del 3D, Avatar no aportó ninguna novedad que no existiera antes.
      </article>
      <article class="four columns white">
        <img alt="Venom" class="twelve columns" src="https://i0.wp.com/elpalomitron.com/wp-content/uploads/2018/10/VENOM-BANNER-El-Palomitr%C3%B3n.jpg" />
        Con Marvel Studios monopolizando cada vez más el mercado de los superhéroes, los otros jugadores (entre ellos
        Sony) se ven obligados a cambiar de estrategia o ser engullidos. La única dirección que tomar para ellos es la
        opuesta al MCU, películas atrevidas, audaces, que no parezcan hechas a partir de una plantilla diseñada en el
        despacho de un ejecutivo de Hollywood, aunque lo sean (Deadpool). En este clima de crisis y de dar luz verde a
        cualquier cosa nace Venom, un proyecto que en sus inicios intentó asociarse con la competencia pero que, al ser
        rechazado</article>
      <article class="four columns white">
        <img alt="Aquaman" class="twelve columns" src="https://2.bp.blogspot.com/-1s2TCCXs4YY/W-7nG0hXuPI/AAAAAAAAsY4/oubuaORXK4QeAcXKzXXGgxAJQrjxZAcdQCLcBGAs/s1600/73522.jpg" />
        Seguimos con la promoción de la próxima película del DCEU ahora con un par de posters que han surgido a la
        superficie, un nuevo banner, y la realización de una nueva proyección especial.

        Desde Rerelease News dan la noticia que Warner Bros. hará un par de proyecciones especiales en distintas
        cadenas de cines estadounidenses, el día 15 de Diciembre, a menos de una semana de su estreno. Los afortunados
        serán los suscritos al sistema Amazon Prime.

        Por otro lado se ha lanzado un nuevo banner, que tienen encabezando la entrada, más dos posters donde uno vemos
        a Aquaman, y el otro al protagonista junto a Mera.</article>

    </section>

    <div class="twelve columns">
      <?php include '../includes/footer.php'?>
    </div>

  </div>
</body>

</html>