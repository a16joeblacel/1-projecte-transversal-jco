<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Mapa Butaques - Cinema JCO</title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">

  <script src="../js/jquery-3.3.1.js"></script>

  <script src="../js/butaques.js"></script>

  <script src="../lib/sweetalert2/dist/sweetalert2.all.min.js"></script>


</head>

<body class="white">

  <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include '../includes/login.php';

    $idpeli = $_POST['id_peli'];
    $idsessio = $_POST['id_sessio'];

    $sqlselect = "select p.nom, s.hora, s.data, p.imatge from pelicula p join sessio s ON p.id = s.id_pelicula where s.id_sessio=$idsessio and p.id=$idpeli";
    $resultselect = mysqli_query($conn, $sqlselect);
    $rowselect = mysqli_fetch_array($resultselect);

    $nompeli = $rowselect[0];
    $horasessio = $rowselect[1];
    $datasessio = $rowselect[2];
    $img = $rowselect[3];

    $sqlreserva = "select r.tipus, r.fila, r.columna from reserva r join sessio s on r.id_sessio = s.id_sessio where s.id_sessio = $idsessio";
    $resultreserva = mysqli_query($conn, $sqlreserva);
    $rowsreserva = mysqli_num_rows($resultreserva);

    $stringreservas = "";

    for ($x = 0; $x < $rowsreserva; $x++) {
        $row = mysqli_fetch_array($resultreserva);

        if ($x == 0) {
            $stringreservas = $row[0] . "-" . $row[1] . "-" . $row[2] . "/";
        } else {
            $stringreservas = $stringreservas . $row[0] . "-" . $row[1] . "-" . $row[2] . "/";

        }
    }

} else {

    echo "Has accedit a aquesta pàgina de manera incorrecta";

    echo "<form action='../index.php'><input type='submit' value='Tornar'/></form>";

    exit();
}

?>
  <div class="container gris sombra butaques">
    <!-- Cabecera -->
    <header class="white twelve columns">
      <!-- Logo del cine-->
      <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO" /></a>
      </div>
      <!-- Nav -->
      <nav class="ten columns">
        <div class="twelve ">
          <?php include '../includes/nav.php'?>
        </div>
      </nav>
    </header>


    <!--- Sala de cine, butaques -->
    <section class="section-but twelve columns">

      <h2>
        <?php echo $nompeli ?>
      </h2>
      <h3>
        <?php echo $datasessio . " | " . $horasessio . " h" ?>
      </h3>
      <hr>
      <div class="diaespectador"></div>
      <article class="article-but twelve columns">
        <div class="pantalla eight columns"><img src="../img/pantalla.png" alt="Pantalla "></div>
        <table class="mapabutaca white eight columns">

        </table>
        <div class="four columns resum butacasd white">
          <img src="../img/butaca.png" class="four columns" alt="Butaca normal">
          <img src="../img/butacavip.png" class="four columns" alt="Butaca normal">
          <img src="../img/butacaelegida.png" class="four columns" alt="Butaca normal"><br>
          <span class="four columns">Butaca normal</span>
          <span class="four columns">Butaca VIP</span>
          <span class="three columns">Butaca sel·lec.</span>
        </div>
        <!-- Form de mostrar dades -->
        <div class="four columns resum white">
          <form class='form-comprar'  action=formcompra.php method="POST">
          <br>
          Preu (€): <br>
          <input type="text" class="preutotal" value="0" disabled>
          <br>Quantitat Entrades: <br>
          <input type="text" class="entradatotal" name="quantitat " value="0" disabled>
          <br>Entrades Seleccionades: <input type="text" class="entradas" value="" disabled>
          <br><br>
          <input type="hidden" name="id_peli" value="<?php echo $idpeli; ?>">
          <input type="hidden" name="id_sessio" value="<?php echo $idsessio; ?>">
          <input type="hidden" name="imatge" value="<?php echo $img; ?>">
          <input type="hidden" name="nom" value="<?php echo $nompeli; ?>">

          <INPUT class="boton but-comprar" type='submit' value='Comprar'>
        </form>
      </div>


      </article>
    </section>

    <!-- Comprobar dia espectador -->
    <div class="diasoferta" hidden>
      <?php

        $sqloferta="SELECT dia from admin where dia='$datasessio'";
        $resultoferta = mysqli_query($conn, $sqloferta);
        $rowsoferta = mysqli_num_rows($resultoferta);

        while ($row = mysqli_fetch_array($resultoferta)){
          printf($row[0]);
        }

        mysqli_close($conn);
       ?>
    </div>

    <div class="stringButOcupadas" hidden>
      <?php echo $stringreservas ?>
    </div>
    <div class="dataSessio" hidden>
      <?php echo $datasessio ?>
    </div>
    <div class="twelve columns">
      <?php include '../includes/footer.php'?>
    </div>
  </div>

</body>

</html>
