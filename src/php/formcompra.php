<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Formulari Compra - Introduir dades</title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">

  <script src="../js/jquery-3.3.1.js"></script>
  <script src="../js/formcompra.js"></script>
  <script src="../lib/sweetalert2/dist/sweetalert2.all.min.js"></script>

</head>

<body class="white compra">


  <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Añadir conn
    include '../includes/login.php';

    //Recoger datos entrada
    $id_peli = $_POST['id_peli'];
    $nompeli = $_POST['nom'];
    $id_sessio = $_POST['id_sessio'];
    $entradas = $_POST['entradas'];
    $preu = $_POST['preutotal'];
    $quantitat = $_POST['quantitat'];
    $img = $_POST['imatge'];

    //Coger datos sesión
    $sql = "select * from pelicula p join sessio s ON p.id = s.id_pelicula where id_sessio='$id_sessio'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_array($result);
    $hora = $row['hora'];
    $data = $row['data'];

    mysqli_close($conn);

} else {

    echo "Has accedit a aquesta pàgina de manera incorrecta";

    echo "<form action='../index.php'><input type='submit' value='Tornar'/></form>";

    exit();
}

?>


    <!-- Main - Padre -->
    <div class="container gris sombra">
      <!-- Cabecera -->
      <header class="white twelve columns">
        <!-- Logo del cine-->
        <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO"></a>
        </div>
        <!-- Nav -->
        <nav class="ten columns">
          <div class="twelve ">
            <h2>Comprar entrades</h2>
          </div>
        </nav>
      </header>
      <!--Form per comprar la entrada -->
      <form action="../php/formcompravalidacio.php" class="formcompr-validacion five columns form borde1" method="post">
        <div class="twelve columns">
          <label>Nom:</label><input type="text" class="campo campnom" name="nomC" required>
          <br>
          <label>Primer Cognom:</label> <input class="campo campcognom" type="text" name="cognom" required>
          <br>
          <label>Data de naixement:</label> <input class="campo campdatanaix" type="date" name="data_naix" required>
          <br>
          <label>E-mail:</label> <input type="email" class="campo campemail" name="email" required>
          <br>
          <label>Telèfon:</label> <input type="tel" class="campo camptlf" name="tlf">
          <br>

          <!--- Variables en ocult per passar dades -->
          <input hidden name="id_peli" value="<?php echo $id_peli; ?>">
          <input hidden name="nom" value="<?php echo $nom; ?>">
          <input hidden name="id_sessio"  value="<?php echo $id_sessio; ?>">
          <input hidden name="entradas" value="<?php echo $entradas; ?>">
          <input hidden name="preu" value="<?php echo $preu; ?>">
          <input hidden name="quantitat" value="<?php echo $quantitat; ?>">
          <input hidden name="imatge" value="<?php echo $img; ?>">
          <input hidden name="hora" value="<?php echo $hora; ?>">
          <input hidden name="data" value="<?php echo $data; ?>">

          <input type="submit" class="btcompra" value="Submit">
        </div>
      </form>

      <!-- Div que mostra la info de la entrada, cine , etc-->
      <div class="seven columns form borde1">
        <div class="ficha_peli twelve columns info white">
          <img class="four columns" src="../<?php echo $img; ?>" alt="Foto cartelera">
          <p class="eight columns gris">
            <label> Película: </label>
            <?php echo $nompeli; ?><br><br>
            <label>Cinema: </label>
            Cinemes JCO<br><br>
            <label>Dia: </label>
            <?php echo $data; ?><br>
            <label>Sessió:</label>
            <?php echo $hora; ?>h<br>
            <br>
            <label>Num. entrades:</label>
            <?php echo $quantitat; ?><br>
            <label>Butaques seleccionades: </label>
            <?php echo $entradas; ?><br>
            <br>
            <label>Preu:</label>
            <?php echo $preu; ?>€<br><br>
          </p>
        </div>
      </div>
      <div class="twelve columns">
        <?php include '../includes/footer.php'?>
      </div>
    </div>

  </body>

</html>