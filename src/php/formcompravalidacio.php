<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

// Si entra pel formulari
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include '../includes/login.php';

    // Obtenim dades
    $id_peli = trim($_POST['id_peli']);
    $nompeli = trim($_POST['nom']);
    $id_sessio = trim($_POST['id_sessio']);
    $entradas = trim($_POST['entradas']);
    $preu = trim($_POST['preu']);
    $data = trim($_POST['data']);
    $img = trim($_POST['imatge']);
    $hora = trim($_POST['hora']);
    $quantitat = trim($_POST['quantitat']);

    $nom = trim($_POST["nomC"]);
    $cognom = trim($_POST["cognom"]);
    $data_naix = trim($_POST["data_naix"]);
    $email = trim($_POST["email"]);
    $tlf = trim($_POST["tlf"]);

    $errorstring = "";

    if (empty($nom)) {

        $errorstring = $errorstring . "nom+";

    }if (empty($cognom)) {

        $errorstring = $errorstring . "cognom+";

    }if (empty($data_naix)) {

        $errorstring = $errorstring . "data_naix+";

    }if (empty($email)) {

        $errorstring = $errorstring . "email+";

    }if (empty($tlf)) {

        $errorstring = $errorstring . "tlf+";

    }if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

        $errorstring = $errorstring . "email+";

    }if (!is_numeric($tlf)) {

        $errorstring = $errorstring . "tlf+";

    }

    if (!empty($errorstring)) {
        mysqli_close($conn);
        header("Location: error.php?error=$errorstring");
        exit();
    }
    // Comprovamos si el usuario tiene una reserva ya
    $selectClient = "select * from reserva where id_client = (select id from client where email='$email') and data>='$data'";
    $resultClient = mysqli_query($conn, $selectClient);
    $rows = mysqli_num_rows($resultClient);

    if ($rows != 0) {
        mysqli_close($conn);
        header("Location: revisarEntrada.php?email=$email");
        exit();
    }

    //Dividir asientos cada uno en una variable
    $asientos = explode(',', $entradas);

    for ($i = 0; $i < $quantitat; $i++) {
        //Separar tipo, filas y columnas
        list($tipus, $butaca) = explode("#", $asientos[$i]);
        list($fila, $columna) = explode("-", $butaca);

        // Comprovamos que nadie haya ocupada alguna de nuestras entradas seleccionadas
        $selectButacas = "SELECT * FROM reserva WHERE fila = '$fila' AND columna = '$columna' AND data='$data'";
        $resultButacas = mysqli_query($conn, $selectButacas);
        $rowsButacas = mysqli_num_rows($resultButacas);

        if ($rowsButacas !== 0) {
            mysqli_close($conn);
            header("Location: error.php?error=1");
            exit();
        }

    }

//insert del client
    $insertClient = "INSERT INTO client (email, nom, cognom, dataNaixement, telefon )
VALUES ('$email', '$nom', '$cognom', '$data_naix', '$tlf')";

    mysqli_query($conn, $insertClient);

    $selectIdClient = "SELECT id from client where email='$email'";
    $id_client = mysqli_query($conn, $selectIdClient);
    $row = mysqli_fetch_array($id_client);
    $id_client = $row['id'];

    for ($i = 0; $i < $quantitat; $i++) {

        //Separar tipo, filas y columnas
        list($tipus, $butaca) = explode("#", $asientos[$i]);
        list($fila, $columna) = explode("-", $butaca);

        //insert de cada una de las entradas. Una a una.
        $insertReserva = "INSERT INTO reserva (tipus, data, fila, columna, id_client, id_sessio)
  VALUES('$tipus', '$data', '$fila', '$columna', '$id_client', '$id_sessio')";

        if (!mysqli_query($conn, $insertReserva)) {
            mysqli_close($conn);
            header("Location: error.php");
            exit();
        }

    }

// Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function

//Cargar clase fpdf
    require '../lib/fpdf/fpdf.php';
//Load Composer's autoloader
    require '../lib/vendor/autoload.php';

//Creacion pdf
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', '15');
    $pdf->Image('../img/logo.png', 5, 5, 50);
    $pdf->Cell(90, 10, 'Cinema JCO', 2, 1, 'R');
    $pdf->Cell(90, 10, "", 0, 1);
    $pdf->Cell(190, 10, $nompeli, 0, 1, 'C');
    $pdf->Cell(190, 10, "Sessio: " . $hora . " h", 0, 1, 'C');
    $pdf->Cell(190, 10, "Dia: " . $data, 0, 1, 'C');
    $pdf->SetFont('Arial', '', '12');
    $pdf->Cell(35, 10, "Nom: ", 0, 0);
    $pdf->Cell(90, 10, $nom, 0, 1);
    $pdf->Cell(35, 10, "Cognom: ", 0, 0);
    $pdf->Cell(90, 10, $cognom, 0, 1);
    $pdf->Cell(35, 10, "E-mail: ", 0, 0);
    $pdf->Cell(90, 10, $email, 0, 1);
    $pdf->Cell(35, 10, "Telefon: ", 0, 0);
    $pdf->Cell(90, 10, $tlf, 0, 1);
    $pdf->Cell(35, 10, "Entradas: ", 0, 0);
    $pdf->Cell(90, 10, $entradas, 0, 1);
    $pdf->Cell(35, 10, "Quantitat:  ", 0, 0);
    $pdf->Cell(90, 10, $quantitat, 0, 1);
    $pdf->Cell(35, 10, "Preu: ", 0, 0);
    $pdf->Cell(90, 10, $preu, 0, 1);

    $output = $pdf->Output("S");

    $mail = new PHPMailer(true); // Passing `true` enables exceptions
    try {
        //Server settings

        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = 'cinemajco@gmail.com'; // SMTP username
        $mail->Password = 'joelcrisoriol12345'; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587; // TCP port to connect to

        //Recipients
        $mail->setFrom('cinemajco@gmail.com', 'cinemaJCO');
        $mail->addAddress($email); // Add a recipient

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->addStringAttachment($output, 'entradesCineJCO.pdf', 'base64', 'application/pdf');

        //Content
        $mail->isHTML(true); // Set email format to HTML
        $mail->Subject = 'Has rebut les teves entrades. Missio complerta!';
        $mail->Body = "Hola,<br><br>Has rebut les teves entrades... <b>Ja pots gaudir del millor cinema!</b><br>
    Mostra directament al porter en la pantalla del teu mòbil qualsevol dels codis de barres que hauràs rebut en aquest correu, guanyaràs temps i evitaràs tràmits innecessaris.
    També pots descarregar i imprimir les teves entrades i portar-les al cine. Les trobaràs al document adjunt que t'enviem.
    En qualsevol cas, vine i sent la màxima immersió. <br>Moltes gràcies de part del equip <b>CinemaJCO</b>";

        $mail->AltBody = "Hola, Has rebut les teves entrades... Ja pots gaudir del millor cinema! Mostra directament al porter en la pantalla del teu mòbil qualsevol dels codis de barres que hauràs rebut en aquest correu, guanyaràs temps i evitaràs tràmits innecessaris.
    També pots descarregar i imprimir les teves entrades i portar-les al cine. Les trobaras al document adjunt que t'enviem.
    En qualsevol cas, vine i sent la màxima immersió. Moltes gràcies de part del equip CinemaJCO";

        $mail->send();

        mysqli_close($conn);
        header("Location: confirmacio.php?email=$email");
        exit();

    } catch (Exception $e) {
        echo "El missatge no s'ha pogut enviar, codi error: ", $mail->ErrorInfo;
    }

} else {

    echo "Has accedit a aquesta pàgina de manera incorrecta";

    echo "<form action='../index.php'><input type='submit' value='Tornar'/></form>";

}