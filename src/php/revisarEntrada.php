<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/colores.css">
    <link rel="stylesheet" href="../css/fuentes.css">


    <title>Revisar entrades - Cinema JCO</title>
</head>

<body class="white">
    <!-- Main - Padre -->
    <div class="container gris sombra">
        <!-- Cabecera -->
        <header class="white twelve columns">
            <!-- Logo del cine-->
            <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO"></a>
            </div>
            <!-- Nav -->
            <nav class="ten columns">
                <div class="twelve ">
                    <?php include '../includes/nav.html'?>
                </div>
            </nav>
        </header>

        <section class='contenedor five columns'>

            <article>


                <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $email = $_POST['emailentradas'];

} else if ($_SERVER["REQUEST_METHOD"] == "GET") {

    $email = $_GET['email'];

}

include '../includes/login.php';

$data = date('Y-m-d');

//Info del client
$sql = "select * from reserva where id_client = (select id from client where email='$email') and data >= '$data'";
if ($result = mysqli_query($conn, $sql)) {
} else {
    echo ("Error description: " . mysqli_error($conn));
}

$rows = mysqli_num_rows($result);
$arrayEntradas = [];

for ($x = 0; $x < $rows; $x++) {

    $row = mysqli_fetch_array($result);
    $id = $row['id_sessio'];
    $dataentrada = $row['data'];
    $tipus = $row['tipus'];
    $fila = $row['fila'];
    $columna = $row['columna'];

    array_push($arrayEntradas, $fila . "-" . $columna);
}
// Info de la sessió
$sql = "Select * from sessio where id_sessio='$id'";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($result);
$id_peli = $row['id_pelicula'];
//info de la peli
$sql = "SELECT * from pelicula where id='$id_peli'";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_array($result);
$nom = $row['nom'];

echo "<div class=''><p>Informació de <strong>" . $email . "</strong><hr><br>";
echo "<br>Tens entrades pel<strong>" . $dataentrada . "</strong> en la película <strong>" . $nom . "</strong> <br><br>";

for ($x = 0; $x < count($arrayEntradas); $x++) {

    $arraySeparado = explode("-", $arrayEntradas[$x]);

    $fila = $arraySeparado[0];
    $columna = $arraySeparado[1];

    echo "Tens una entrada per la <strong>fila " . $fila . " columna " . $columna . "</strong><br>";

}
echo "<br></div> ";
mysqli_close($conn);

?>
            </article>

        </section>

    </div>
</body>

</html>