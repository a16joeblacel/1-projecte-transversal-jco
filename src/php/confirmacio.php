<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Confirmació - Cinema JCO</title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">

</head>

<body class="admin">
  <!-- Main - Padre -->
  <div class="container gris sombra">
    <!-- Cabecera -->
    <header class="white twelve columns">
      <!-- Logo del cine-->
      <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO"></a>
      </div>
      <!-- Nav -->
      <nav class="ten columns">
        <div class="twelve ">
          <?php include '../includes/nav.php'?>
        </div>
      </nav>
    </header>

    <div class="twelve columns">
      <section>
        <article class="twelve columns white">
          <h2>Confirmació de la compra</h2>
          <div class="two columns"><br></div>
          <img class="three columns complete" src="../img/ok.png" alt="">
          <div class="one columns"><br></div>
          <div class="six columns">
            <?php
            $email = $_GET['email'];
            echo "<b>" . $email . "</b>";
            ?>

            <p>Hem rebut el teu pagament, ja tens les entrades al teu correu electrònic.</p>
          </div>
        </article>
      </section>
      <div class="twelve columns">
        <?php include '../includes/footer.php'?>
      </div>
    </div>
  </div>

</body>

</html>
