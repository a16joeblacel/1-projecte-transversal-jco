<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Preu - Cinema JCO</title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">
</head>

<body class="white">
  <!-- Main - Padre -->
  <div class="container gris sombra">
    <!-- Cabecera -->
    <header class="white twelve columns">
      <!-- Logo del cine-->
      <div class="two columns"><a href="../index.php"><img class="twelve columns" src="../img/logo.png" alt="Cinema JCO"></a>
      </div>
      <!-- Nav -->
      <nav class="ten columns">
        <div class="twelve ">
          <?php include '../includes/nav.php'?>
        </div>
      </nav>
    </header>
    <section class="twelve columns">
      <article class="twelve columns ">
        <h2>Preus:</h2>
        <hr>
        <p class="white preus">
          <strong>Entrada general:</strong> 6€<br><br>
          <strong>Entrada VIP:</strong> 8€<br>
        </p>
      </article>
      <article class="twelve columns">
        <h2>Descomptes dia espectador:</h2>
        <hr>
        <p class="white preus">
          <strong>General:</strong> 4€<br><br>
          <strong>VIP:</strong> 6€<br>

        </p>
      </article>
    </section>
    <div class="twelve columns">
      <?php include '../includes/footer.php'?>
    </div>
  </div>
</body>

</html>