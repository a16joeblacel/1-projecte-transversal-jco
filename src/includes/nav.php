<style>
  nav ul{
    margin-top:5%;
  }
  nav li{
    display: inline;
    text-decoration: none;
    margin-left: 4%;
    padding:4%;
  }
  .nav:hover{
    background-color: orange;
  }
  a{
    text-decoration: none;
    color:black;
  }
</style>

<?php
$url = $_SERVER["REQUEST_URI"];
$host= "labs.iam.cat/~a15orisolber/src/";
if ($url == "http://" . $host . "/index.php") {
    ?>
<!--- Nav para la pag principal-->
<div class="six columns">
  <ul>
    <li class="nav"><a href="index.php">Inici</a> </li>
    <li class="nav"><a href="php/actualidad.php">Actualitat</a></li>
    <li class="nav"><a href="php/preu.php">Preus</a></li>
  </ul>
</div>

<!-- Buscar entrades -->
<div class="five columns buscar">
  <form class="twelve columns" action="php/revisarEntrada.php" method="POST">
    <input type="email" class="ten columns" name="emailentradas" style="font-size:10px" value="" placeholder="Introdueix el teu correu"
      required />
    <input type="submit" class="two columns" style="font-size:10px" value="Buscar" />
  </form>
</div>
<?php } else {
    ?>
<!--- Nav para el resto -->
<div class="six columns">
  <ul>
    <li class="nav"><a href="<?php echo 'http://' . $host . 'index.php'; ?>">Inici</a> </li>
    <li class="nav"><a href="<?php echo 'http://' . $host . 'php/actualidad.php'; ?>">Actualitat</a></li>
    <li class="nav"><a href="<?php echo 'http://' . $host . 'php/preu.php'; ?>">Preus</a></li>
  </ul>
</div>

<!-- Buscar entrades -->
<div class="five columns buscar">
  <form class="twelve columns" action="<?php echo 'http://' . $host . 'php/revisarEntrada.php'; ?>" method="POST">
    <input type="email" class="six columns" name="emailentradas" style="font-size:10px" value="" placeholder="Introdueix el teu correu"
      required />
    <input type="submit" class="five columns" style="font-size:10px" value="Buscar" />
  </form>
</div>

<?php
}?>