<?php

$db_hostname = 'localhost';
$db_database = 'cinema';
$db_username = 'root';
$db_password = '';

// Create connection
$conn = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
$conn->set_charset('utf8');
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>
