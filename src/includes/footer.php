<style>
  .footer {
    padding-top: 20px;
    background: #63639B;
    color: white;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
    margin-top: 20px;
    overflow: hidden;
    padding-bottom:10px;
}
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 100px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 2;
  right: -30%;
  margin-left: -60px;
  opacity: 0;
  transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  bottom: 85%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
font-size:10px;
  visibility: visible;
  opacity: 1;
}
  </style>
  <?php
    $url= $_SERVER["REQUEST_URI"];
    $host= "labs.iam.cat/~a15orisolber/src/";
    if($url=="http://".$host."/index.php"){
   ?>

  <footer class="footer twelve columns">
    <div class="ten columns  fder">Copyright Cinema JCO © 2018 Tots Els Drets Reservats</div>
    <div class="two columns"><a href ="admin/admin.php">Menú Admin</a></div>
  </footer>
  <?php }
  else {?>
  <footer class="footer twelve columns">
    <div class="ten columns  fder">Copyright Cinema JCO © 2018 Tots Els Drets Reservats</div>
    <div class="two columns tooltip"><a  href="<?php echo 'http://'.$host.'admin/admin.php';?>" >Menú Admin</a>  <span class="tooltiptext">ausias/ausias</span> </div>
  </footer>
  <?php
    }
  ?>
