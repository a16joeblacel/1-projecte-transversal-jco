<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Cinema JCO</title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">

  <script src="../js/jquery-3.3.1.js"></script>
  <script src="../js/admin-butaques.js"></script>
  <script src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
  <div class="container gris sombra butaques">
  
    <?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    include '../includes/login.php';

    $data = $_POST['data'];

    // Obtenim id sessio per consultar reservas de la sessió d'aquell dia
    $selectSessio = "select id_sessio from sessio where data ='$data'";
    $resultSessio = mysqli_query($conn, $selectSessio);
    $row = mysqli_fetch_array($resultSessio);
    $id_sessio = $row['id_sessio'];

    // Obtenim totes les reserves de la sessió
    $sqlreserva = "select r.tipus, r.fila, r.columna from reserva r join sessio s on r.id_sessio = s.id_sessio where s.id_sessio = $id_sessio";
    $resultreserva = mysqli_query($conn, $sqlreserva);
    $rowsreserva = mysqli_num_rows($resultreserva);

    $stringreservas = "";

    for ($x = 0; $x < $rowsreserva; $x++) {
        $row = mysqli_fetch_array($resultreserva);

        if ($x == 0) {
            $stringreservas = $row[0] . "-" . $row[1] . "-" . $row[2] . "/";
        } else {
            $stringreservas = $stringreservas . $row[0] . "-" . $row[1] . "-" . $row[2] . "/";
        }

    }

} else {
    // Si algú accedeix sense posar una data pel form o posant l'url directe
    echo "Has accedit a aquesta pàgina de manera incorrecta";

    echo "<form action='admin.php'><input type='submit' value='Tornar'/></form>";

    exit();}

?>

    <section class="section-but">
      <article class="article-but">
        <table class="mapabutaca">
        </table>
        <h2>Informe</h2>
        <div class="diaespectador"></div>
        <div>
          <table>
            <thead>
              <td>Tipus</td>
              <td>Ocupació</td>
              <td>Recaudació</td>
            </thead>
            <tr>
              <td><strong>Normal</strong></td>
              <td class="ocupacionormal">0</td>
              <td class="recaudacionormal">0</td>
            </tr>
            <tr>
              <td><strong>Vip</strong></td>
              <td class="ocupaciovip">0</td>
              <td class="recaudaciovip">0</td>
            </tr>
            <tr>
              <td><strong>Total</strong></td>
              <td class="ocupaciototal">0</td>
              <td class="recaudaciototal">0</td>
            </tr>
          </table>

        </div>
        <div id="graphics-sectors-ocupacio">
        </div>
        <div id="graphics-barras-ocupacio">
        </div>
        <div id="graphics-sectors-recaudacio">
        </div>
        <div id="graphics-barras-recaudacio">
        </div>
      </article>
    </section>

    <!-- Comprobar dia espectador -->
    <div class="diasoferta" >
      <?php

        $sqloferta="SELECT dia from admin where dia='$data'";
        $resultoferta = mysqli_query($conn, $sqloferta);
        $rowsoferta = mysqli_num_rows($resultoferta);

        if ($rowsoferta <= 0 ){
            echo 0;
          
        }
        while ($row = mysqli_fetch_array($resultoferta)){
          if ($row[0] == $data){
            echo true;
          } 
        }


     
        
       ?>
    </div>
    <!-- Div amb butaques ocupades per pasar-li al javascript -->
    <div class="stringButOcupadas" hidden>
      <?php echo $stringreservas ?>
    </div>

    <div class="div-data" hidden>
      <?php echo $data ?>
      </div>

  </div>


</body>

</html>