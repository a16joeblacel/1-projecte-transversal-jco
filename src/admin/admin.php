<!DOCTYPE html>
<html lang="es">

<head>
  <?php include '../includes/login.php';?>
  <title>Panell d'Administració </title>
  <link rel="stylesheet" href="../css/reset.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/skeleton.css">
  <link rel="stylesheet" href="../css/colores.css">
  <link rel="stylesheet" href="../css/fuentes.css">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body class="admin">
  <div class="container gris sombra">
    <!-- Control ocupació i recaudació-->
    <section>
      <article class="white">
        <h3>Ocupació per dia:</h3>
        <form action="admindia.php" method="post">
          Data: <input type="date" name="data"> <input type="submit" value="Buscar">
        </form>
      </article>
    </section>

    <!-- Administració del cine-->
    <section>
      <article class="white">
        <h3>Administració:</h3>
        <strong>Habilitar dia de l'espectador (per defecte només dimecres):</strong>
        <?php

/* Comprobar si es día del espectador avui */
$date = date("Y-m-d");
$sql = "SELECT * from admin";
$result = mysqli_query($conn, $sql);
$rows = mysqli_num_rows($result);
$estado = "<strong>desactivat</strong>. No és día de l'espectador.";
for ($x = 0; $x < $rows; $x++) {
    $row = mysqli_fetch_array($result);
    $dia = $row['dia'];
    if ($date == $dia) {
        $estado = "<strong>activat</strong>. Avui és día de l'espectador.";
    }

}
?>
        <br>Estat actual:
        <?php echo $estado; ?>
        <form action="diaespectador.php" method="post">
          Nou dia de l'espectador: <input type="date" name="data">
          <input type="submit" name="submit" value="IR" />
        </form>
      </article>
    </section>

    <div class="volver">
      <input class="five columns" type="button" onClick="window.location='../index.php'" value="Tornar a la web">
    </div>
  </div>
</body>

</html>