<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Cinema JCO</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/colores.css">
  <link rel="stylesheet" href="css/fuentes.css">

  <script src="js/jquery-3.3.1.js"></script>
  <script src="js/banner.js"></script>
  <script src="js/calendari.js"></script>

</head>

<body class="white">
  <!-- Main - Padre -->
  <main class="container gris sombra">
    <!-- Cabecera -->
    <header class="white twelve columns">
      <!-- Logo del cine-->
      <div class="two columns"><a href="index.php"><img class="twelve columns" src="img/logo.png" alt="Cinema JCO"></a>
      </div>
      <!-- Nav -->
      <nav class="ten columns">
        <div class="twelve ">
          <?php include 'includes/nav.php'?>
        </div>
      </nav>
    </header>
    <!--- Banner y carrusel  -->
    <div class="banner twelve column gris">
      <!-- Banner sin carrusel <img id="imagen" src="img/banners/2_B.jpg" alt="Banner"/>-->
      <?php include "includes/carrusel.html";?>
    </div>

    <?php
include './includes/login.php';

$newday = $_GET['day'];
if (empty($newday)) {
    $date = date('Y-m-d');
    $dateESP = date("d-m-Y");
} else {
    $date = date('Y-m-' . $newday);
    $dateESP = date($newday . "-m-Y");

}

$sql = "select * from pelicula p join sessio s ON p.id = s.id_pelicula where s.data = '$date'";
$result = mysqli_query($conn, $sql);
$rows = mysqli_num_rows($result);

//Coger info
$row = mysqli_fetch_array($result);
$id = $row['id'];
$nom = $row['nom'];
$img = $row['imatge'];
$descripcio = $row['descripcion'];
$id_sessio = $row['id_sessio'];
$hora = $row['hora'];
$data = $row['data'];
$trailer = $row['trailer'];

$selectnompeli = "select p.nom, s.data from pelicula p join sessio s on s.id_pelicula = p.id;";
$resultnompeli = mysqli_query($conn, $selectnompeli);
$files = mysqli_num_rows($resultnompeli);

mysqli_close($conn);

?>

    <!--- Info de la peli -->
    <section>
      <h2 class="twelve columns">Avui:
        <?php echo $nom; ?>
      </h2>
      <hr class="eleven column">

      <!--- Primer apartado-->
      <article>
        <div class="ficha_peli white twelve columns">
          <!-- Imagen cartelera-->
          <div class="three columns">
            <img src="<?php echo $img; ?>" alt="Imagen cartelera">
          </div>
          <!-- Trailer peli -->
          <div class="trailer nine columns">
            <iframe class="twelve columns" src="<?php echo $trailer; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen></iframe>
          </div>
        </div>
        <!--- Segundo apartado -->
        <div class="cartelera ficha_peli white twelve columns">
          <article>
            <strong>Sinopsis:</strong>
            <p class="twelve column">
              <?php echo $descripcio; ?>
            </p>
          </article>
        </div>


        <!--- Compra -->
        <form class="two column sessio" action="php/mapabutaques.php" method="post">
          <!-- Hora sessió-->
          <?php
$hoy = date("d-m-Y");
if ($dateESP >= $hoy && $dateESP != "") {
    ?>
          <table class="sessio">
            <tr>
              <td>Sessió disponible // <strong>
                  <?php echo $dateESP; ?></strong></td>
            </tr>
            <td>
              <?php echo $hora; ?>
            </td>
          </table>
          <!-- Valor ocults a passar per comprar entrada -->
          <input name="id_peli" value="<?php echo $id ?>" type="hidden">
          <input name="id_sessio" value="<?php echo $id_sessio ?>" type="hidden">
          <div class="twelve columns">
            <input class="boto" type="submit" value="Comprar" />
          </div>
          <?php
}
?>
        </form>

        <!--- Calendari -->
        <div class="twelve columns tcalendari">
          <table class=" calendari">
          </table>
        </div>

        <div hidden class="array-pelis">
          <?php
$arrayPelis = [];
for ($x = 0; $x < $files; $x++) {
    $row = mysqli_fetch_array($resultnompeli);
    echo $row[0];
    echo "/";
    echo $row[1] . "#";
}
?>

        </div>

      </article>
    </section>
    <div class="twelve columns">
      <?php include 'includes/footer.php'?>
    </div>

  </main>
</body>

</html>