// Mostra una alerta
function showAlert(error) {

    Swal({
        title: 'Camp ' + error + ' incorrecte!',
        type: 'error',
        confirmButtonText: 'Ok'
    })

}

// Validar email
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// Valida telèfon, només comproba si té 9 dígits i si no hi ha cap lletra
function validatePhone(tlf) {

    if (isNaN(tlf) && isNaN(tlf - 0)) {
        return false
    } else if (tlf.length != 9) {
        return false
    }

    return true
}

$(document).ready(function () {

    // Parem la execució del botó per validar camps
    $('.formcompr-validacion').submit(function (evt) {
        evt.preventDefault();

        // Obtenir camps
        let nom = $(".campnom").val()
        let cognom = $(".campcognom").val()
        let datanaix = $(".campdatanaix").val()
        let email = $(".campemail").val()
        let tlf = $(".camptlf").val()

        let date = new Date(datanaix)
        let datactual = new Date();

        // Validacions de camps, si hi ha error mostra alerta, sinó continua l'execució del botó
        if (nom == "") {
            showAlert("nom")
        } else if (cognom === "") {
            showAlert("cognom")
        } else if (datanaix === "") {
            showAlert("data naixement")
        } else if (email === "") {
            showAlert("email")
        } else if (tlf === "") {
            showAlert("telèfon")
        } else if (date >= datactual) {
            showAlert("data naixement")
        } else if (!validateEmail(email)) {
            showAlert("email")
        } else if (!validatePhone(tlf)) {
            showAlert("telèfon")
        } else {
            evt.currentTarget.submit();
        }



    })
})