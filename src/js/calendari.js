// Funció per generar un calendari a partir d'una data
function generaCalendario(año, mes, dia) {

    let calendariostring = '';

    let date = new Date(año, mes, dia)
    var numeroDias = diasEnUnMes(date.getMonth() + 1, date.getFullYear());


    let dateDay1 = new Date(date.getFullYear(), date.getMonth(), 1);


    let dayMonthStart = dateDay1.getUTCDay();

    var dias = ["Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"];



    calendariostring += "<thead><tr>"

    for (j = 0; j < 7; j++) {
        calendariostring += "<td>" + dias[j] + "</td>"
    }

    calendariostring += "</tr></thead><tbody>"


    let numeroAux = 1;

    let check = true;

    for (x = 0; x < 6; x++) {
        calendariostring += "<tr>";
        for (z = 0; z < 7; z++) {

            if (check) {
                z = dayMonthStart
                check = false;

                for (j = 0; j < dayMonthStart; j++) {
                    calendariostring += '<td class="outofmonth white hover"> </td>'
                }

            }

            if (numeroAux <= numeroDias) {

                if (dia == numeroAux) {

                    calendariostring += "<td class='actualday lila " + numeroAux + "'>" + numeroAux + "</td>"


                } else {
                    calendariostring += "<td class='normalday white hover " + numeroAux + "'>" + numeroAux + "</td>"
                }
                numeroAux++

            } else {
                calendariostring += "<td class='outofmonth white hover'></td>"

            }
        }
        calendariostring += "</tr>"
    }
    calendariostring += "</tbody></table>"

    return calendariostring;

}

// Retorna la quantitat de dies d'un mes d'un any
function diasEnUnMes(mes, año) {
    return new Date(año, mes, 0).getDate();
}

$(document).ready(function () {

    var data = new Date();

    var año = data.getFullYear()
    var mes = data.getMonth()
    var dia = data.getDate()

    mes++

    // Generem el calendari en la taula
    let tabla = generaCalendario(año, --mes, dia)

    $(".calendari").html(tabla);

    // Quan fagin click en un altre dia carreguem la pàgina amb un data diferent
    $(".normalday").click(function () {

        let valor = $(this).html()

        let dia = valor.split("<")

        window.open('index.php?day=' + dia[0], '_self')

    })

    $(".actualday").click(function () {

        let valor = $(this).html()

        let dia = valor.split("<")

        window.open('index.php?day=' + dia[0], '_self')

    })



    // Obtenim array pelis de l'html
    let array = ($(".array-pelis").html());

    let arrayPelis = array.split("#");

    arrayPelis.splice(-1, 1)

    // Per cada dia afegeix el nom de la peli
    for (let v = 0; v < arrayPelis.length; v++) {

        let arrayDades = arrayPelis[v].split("/");

        let peli = arrayDades[0]
        let data = arrayDades[1]

        let dateParseado = data.split("-")
        let dia = dateParseado[2]

        let diaFormat = dia.replace(/^0+/, '')

        // Afegim el nom de la peli per cada dia
        $("." + diaFormat).append("<p><strong>" + peli + "</strong></p>")
    }
})
