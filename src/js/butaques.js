$(document).ready(function () {

    const preunormal = 6;
    const preuvip = 8
    const preunormalespec = 4;
    const preuvipespec = 6;

    dataSessio = $(".dataSessio").html()

    let date = new Date(dataSessio)

    diaSetmana = date.getDay()

    let diaespectador = false;

    let diaespec = $(".diasoferta").html()

    // Mostrem un missatge si es el dia de l'espectador
    if ((diaespec == dataSessio)||(diaSetmana==3)){
        $(".diaespectador").html("<h2>Dia de l'Espectador</h2>")
        diaespectador = true;
    } 

   
 

    // Pintem el mapa de butaques
    for (let c = 1; c <= 12; c++) {
        $(".mapabutaca").append("<tr>")
        for (let x = 1; x <= 10; x++) {

            // Fila Vip
            if (c == 6) {
                $(".mapabutaca").append("<td columna='" + x + "' fila='" + c + "' class='butaca vip'><img src='../img/butacavip.png'></td>")
            } else {
                $(".mapabutaca").append("<td columna='" + x + "' fila='" + c + "' class='butaca'><img src='../img/butaca.png'></td>")
            }
        }

        $(".mapabutaca").append("</tr>")
    }

    let stringOcupadas = $(".stringButOcupadas").html()

    let arrayParseado = stringOcupadas.split("/")

    // Pintem les butaques reservades, obtenides de la base de dades
    for (let b = 0; b < arrayParseado.length; b++) {
        let arrayEntrada = arrayParseado[b].split("-")

        let tipus;
        let fila;
        let columna;

        for (let n = 0; n < arrayEntrada.length; n++) {

            tipus = arrayEntrada[0]
            fila = arrayEntrada[1]
            columna = arrayEntrada[2]

            // Si es fila 6 la pintem amb imatge diferent per ser vip
            if (fila == 6) {
                let butacaocupada = $("[columna='" + columna + "'][fila='" + fila + "']")
                $(butacaocupada).addClass("reservada")
                $(butacaocupada).html("<img src='../img/butacaocupadavip.png'>")
            } else {
                let butacaocupada = $("[columna='" + columna + "'][fila='" + fila + "']")
                $(butacaocupada).addClass("reservada")
                $(butacaocupada).html("<img src='../img/butacaocupada.png'>")
            }
        }
    }


    let selected = [];

    let preu = 0;
    let quantitat = 0;
    let entradas = "";

    // Per cada click en una butaca cambiem imatge
    $("td").click(function () {

        let columna = $(this).attr("columna")
        let fila = $(this).attr("fila")

        // Si la butaca no ha estat reservada ja
        if (!$(this).hasClass("reservada")) {

            // Si la butaca l'hem seleccionat nosaltres abans
            if ($(this).hasClass("elegida")) {

                if ($(this).hasClass('vip')) {
                    $(this).html("<img src='../img/butacavip.png'>")

                } else {
                    $(this).html("<img src='../img/butaca.png'>")
                }

                $(this).removeClass("elegida")

                quantitat--;
                $('.entradatotal').attr('value', quantitat)

                if (diaespectador) {

                    if ($(this).hasClass('vip')) {
                        preu -= preuvipespec;
                    } else {
                        preu -= preunormalespec;
                    }

                } else {
                    if ($(this).hasClass('vip')) {
                        preu -= preuvip;
                    } else {
                        preu -= preunormal;
                    }
                }

                $('.preutotal').attr('value', preu)

                for (x = 0; x < selected.length; x++) {
                    if ((selected[x] === "N#" + fila + "-" + columna) || (selected[x] === "V#" + fila + "-" + columna)) {
                        selected.splice(x, 1);
                    }
                }

                $('.entradas').attr('value', selected)


                // Si seleccionem una butaca sense marcar
            } else {

                // Si selecciona més de 10 mostrem missatge d'alerta
                if (selected.length >= 10) {

                    Swal({
                        title: 'Màxim 10 entrades!',
                        type: 'warning',
                        confirmButtonText: 'Ok'
                    })

                }
                else {
                    $(this).addClass("elegida")
                    if ($(this).hasClass('vip')) {
                        $(this).html("<img src='../img/butacaelegidavip.png'>")
                    } else {
                        $(this).html("<img src='../img/butacaelegida.png'>")
                    }

                    quantitat++;
                    $('.entradatotal').attr('value', quantitat)

                    // Si es el dia de l'espectador el preu es diferent
                    if (diaespectador) {

                        if ($(this).hasClass('vip')) {
                            preu += preuvipespec;
                            selected.push("V#" + fila + "-" + columna)
                        } else {
                            preu += preunormalespec;
                            selected.push("N#" + fila + "-" + columna)
                        }

                    } else {

                        if ($(this).hasClass('vip')) {
                            preu += preuvip;
                            selected.push("V#" + fila + "-" + columna)
                        } else {
                            preu += preunormal;
                            selected.push("N#" + fila + "-" + columna)
                        }
                    }

                    $('.preutotal').attr('value', preu)

                    $('.entradas').attr('value', selected)

                }

            }
        }

    })



    // Parem el botó de submit per afegir dades en ocult en el formulari i pasar-los a la següent pantalla
    $('.form-comprar').submit(function (evt) {

        // Parem l'execució del botó
        evt.preventDefault();

        let preutotal = $('.preutotal').attr('value')

        if (preutotal == 0) {

            Swal({
                title: 'Selecciona una entrada per poder continuar!',
                type: 'error',
                confirmButtonText: 'Ok'
            })

        } else {
            $(".form-comprar").prepend("<input name='entradas' type='hidden' value=" + selected + ">")
            $(".form-comprar").prepend("<input name='preutotal' type='hidden' value=" + preu + ">")
            $(".form-comprar").prepend("<input name='quantitat' type='hidden' value=" + quantitat + ">")

            // Contínua l'execució del botó
            evt.currentTarget.submit();
        }


    });



})