$(document).ready(function () {

    // Pintem mapa de butaques amb 12 files x 10 columnes
    for (let c = 1; c <= 12; c++) {
        $(".mapabutaca").append("<tr>")
        for (let x = 1; x <= 10; x++) {

            // Fila Vip
            if (c == 6) {
                $(".mapabutaca").append("<td columna='" + x + "' fila='" + c + "' class='butaca vip'><img src='../img/butacavip.png'></td>")
            } else {
                $(".mapabutaca").append("<td columna='" + x + "' fila='" + c + "' class='butaca'><img src='../img/butaca.png'></td>")
            }
        }

        $(".mapabutaca").append("</tr>")
    }


    let datadmin = $(".div-data").html()

    datafinal = new Date(datadmin)
    diaSetmana = datafinal.getDay()

    let diaespectador = false;
    let stringspec = $(".diasoferta").html()

    if (stringspec == 1) {
        $(".diaespectador").html("<h2>Dia de l'Espectador</h2>")
        diaespectador = true;
    } else if (diaSetmana == 3) {
        $(".diaespectador").html("<h2>Dia de l'Espectador</h2>")
        diaespectador = true;
    }




    let stringOcupadas = $(".stringButOcupadas").html()

    let arrayParseado = stringOcupadas.split("/")

    let totalocupacionormal = 0;
    let totalocupaciovip = 0;
    let recaudacionormal = 0;
    let recaudaciovip = 0;

    // Pintem les butaques, obtenides de la taula reserva de la base de dades
    for (let b = 0; b < arrayParseado.length; b++) {
        let arrayEntrada = arrayParseado[b].split("-")

        let tipus;
        let fila;
        let columna;

        tipus = arrayEntrada[0]
        fila = arrayEntrada[1]
        columna = arrayEntrada[2]

        let butacaocupada = $("[columna='" + columna + "'][fila='" + fila + "']")
        $(butacaocupada).addClass("reservada")
        $(butacaocupada).html("<img src='../img/butacaocupada.png'>")

        // Si es vip el preu és major i si es dia espectador el preu és menor en ambdós casos
        if (diaespectador) {

            if (tipus === 'V') {
                totalocupaciovip++;
                recaudaciovip += 6;

            } else if (tipus === 'N') {
                totalocupacionormal++;
                recaudacionormal += 4;

            }

        } else {

            if (tipus === 'V') {
                totalocupaciovip++;
                recaudaciovip += 8;

            } else if (tipus === 'N') {
                totalocupacionormal++;
                recaudacionormal += 6;

            }

        }


    }

    // Pintem la ocupació en cada td de la taula
    $(".ocupacionormal").html(totalocupacionormal)
    $(".ocupaciovip").html(totalocupaciovip)
    $(".recaudacionormal").html(recaudacionormal)
    $(".recaudaciovip").html(recaudaciovip)
    $(".ocupaciototal").html(totalocupacionormal + totalocupaciovip)
    $(".recaudaciototal").html(recaudaciovip + recaudacionormal)

    // Carreguem els diagrames en un fil apart
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(ChartSectoresOcupacio);
    google.charts.setOnLoadCallback(ChartSectoresRecaudacio);
    google.charts.setOnLoadCallback(ChartBarrasOcupacio);
    google.charts.setOnLoadCallback(ChartBarrasRecaudacio);

    // Diagrama sectors ocupació
    function ChartSectoresOcupacio() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Ocupació'],
            ['Normal', totalocupacionormal],
            ['Buit', 120 - (totalocupacionormal - totalocupaciovip)],
            ['Vip', totalocupaciovip],

        ]);

        var options = { 'title': 'Ocupació sessió', 'width': 400, 'height': 350, colors: ['grey', 'gold', 'brown'] };
        var chart = new google.visualization.PieChart(document.getElementById('graphics-sectors-ocupacio'));


        chart.draw(data, options);
    }

    // Diagrama sectors recaudació
    function ChartSectoresRecaudacio() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Recaudació'],
            ['Normal', recaudacionormal],
            ['Vip', recaudaciovip],
        ]);

        var options = { 'title': 'Recaudació sessió', 'width': 400, 'height': 350, colors: ['grey', 'gold'] };
        var chart = new google.visualization.PieChart(document.getElementById('graphics-sectors-recaudacio'));

        chart.draw(data, options);

    }

    // Diagrama barres ocupació
    function ChartBarrasOcupacio() {
        var data = google.visualization.arrayToDataTable([
            ["Tipus", "Quantitat", { role: "style" }],
            ["Normal", totalocupacionormal, "grey"],
            ["Vip", totalocupaciovip, "gold"],
            ['Buit', 120 - (totalocupacionormal - totalocupaciovip), "brown"],
            ["Total", totalocupacionormal + totalocupaciovip, "black"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);

        var options = {
            title: "Ocupació sessió",
            width: 450,
            height: 350,
            bar: { groupWidth: "95%" },
            legend: { position: "none" },
        };

        var chart = new google.visualization.BarChart(document.getElementById("graphics-barras-ocupacio"));
        chart.draw(view, options);
    }

    // Diagrama barres recaudació
    function ChartBarrasRecaudacio() {
        var data = google.visualization.arrayToDataTable([
            ["Element", "Recaudació", { role: "style" }],
            ["Normal", recaudacionormal, "grey"],
            ["Vip", recaudaciovip, "gold"],
            ["Total", recaudacionormal + recaudaciovip, "black"],
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);

        var options = {
            title: "Recaudació sessió",
            width: 450,
            height: 350,
            bar: { groupWidth: "95%" },
            legend: { position: "none" },
        };

        var chart = new google.visualization.BarChart(document.getElementById("graphics-barras-recaudacio"));
        chart.draw(view, options);
    }

})